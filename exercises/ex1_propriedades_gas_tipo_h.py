#!/usr/bin/env python
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :

from math import *
import numpy as np
import scipy as sp

print("""
Exercício:
Verifcar as características do gás natural, tipo H, especificado no
diapositivo 21, utilizando a tabela de propriedades termodinâmicas
no diapositivo 15.
""")

C = ['C1', 'C2', 'N2', 'C3', 'nC4', 'iC4', 'CO2', 'He', 'nC5', 'iC5', 'nC6']
X = np.array([83.7, 7.6, 5.4, 1.92, .4, .3, .23, .2, .09, .08, .08]) / 1e2
M = np.array([16.043, 30.070, 28.013, 44.096, 58.122, 58.122,\
    44.010, 2.01588, 72.149, 72.149, 86.175]) 
Tc = np.array([191, 305, 126, 370, 425, 408, 304, 33, 470, 460, 507])
Pc = np.array([\
    4599e3, 4872e3, 3396e3, 4251e3, 3796e3, 3629e3, 7377e3, 1348e3,\
    3370e3, 3378e3, 3012e5])
PCI = np.array([\
    33950, 60430, 0, 86420, 112400, 112000, 0, 0, 138400, 138100, 164390])
PCS = np.array([\
    37706, 66066, 0, 93934, 121790, 121400, 0, 0, 149650, 149360, 177550])


print("\ncomposição e respcetivos valores na tabela:")
print("%-4s  %-6s  %-9s  %-4s  %-10s  %-10s  %-10s" % (\
    "comp", "X %", "M kg/kmol", "Tc K", "Pc Pa", "PCI kJ/m3", "PCS kJ/m3"))
for c in zip(C, X * 100, M, Tc, Pc, PCI, PCS):
    print("%-4s  %6.2f  %9.3f  %4.0f  %10.4e  %10.4e  %10.4e" % c)


print("""
#######
##
## Resolução
##
#######
""")

Mg = sum(M*X)
Mar = 28.9625
dg = Mg/Mar
Tpc = sum(X*Tc)
Ppc = sum(X*Pc)

Tg, Pg = 288.15, 101325.
Tn, Pn = 273.15, 101325.
PCIg = sum(X*PCI)
PCSg = sum(X*PCS)

PCIn = PCIg * Tg/Pg * Pn/Tn
PCSn = PCSg * Tg/Pg * Pn/Tn

Wobbe = PCSn / sqrt(dg) * 1e-3

Wobbe15 = Wobbe * Tn/Pn * 101325./(15 + 273.15)

print("Mg    = %g kg/kmol" % Mg)
print("dg    = %g" % dg)
print("Tpc   = %g K" % Tpc)
print("Ppc   = %g bar" % (Ppc / 1e5))
print("PCIg  = %g kJ/m^3" % PCIg)
print("PCSg  = %g kJ/m^3" % PCSg)
print("PCIn  = %g kJ/m^3 (n)" % PCIn)
print("PCSn  = %g kJ/m^3 (n)" % PCSn)
print("Wobbe = %g MJ/m^3 (n)" % Wobbe)
print("Wobbe = %g MJ/m^3 (15 degC)" % Wobbe15)



