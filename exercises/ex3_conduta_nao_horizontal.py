#!/usr/bin/env python3
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :

from math import *
import numpy as np
import scipy as sp
from scipy.optimize import newton


print("""
Numa conduta inclinada (40 deg) com 110~mm de diâmetro e 10~m de comprimento


Considere um troço de 10 m de uma conduta com 110 mm de diâmetro e
uma inclinação de 40 deg, por onde se escoa um gás (d=0.65) a uma
temperatura constante de 15 degC.
A pressão absoluta no final (parte superior) é 3 bar e
e há uma diferença de 150 Pa entre a pressão na entrada e na saída.
Considere uma viscosidade para o gás de mu = 1.0757e-5 Pa·s
e uma aceleração gravítica como 9.8 m/s^2.

1. Qual o sentido do escoamento (assuma Zm = 1).

2. Qual o valor da pressão limite na entrada da conduta para
   que o escoamento mude de direcção.

3. Considerando os efeitos de compressibilidade de um gás real,
   recalcule o sentido do escoamento.

   Nota: faça uso das equações de Garb e de Papp.
""")




Runiv = 8314.41  # J/kg/K
Mar = 28.9625  # kg/kmol
g = 9.8

d = 0.65
Zm = 1.
Tm = 288.15
Ti = Tm
Tj = Tm
Pi = 150 + 3e5
Pj = 3e5


D = 110e-3

L = 10.
sina = sin(radians(40))

mu = 1.0757e-5  # Pa·s



print("""
#######
##
## Resolução
##
#######
""")


M = d * Mar
R = Runiv / M


print("\n" + 32*"-" + "\n1.1: substituir f*L/D*m^2 por Hf e verificar o sinal")


def funHf (Pi, Pj):
    Pm = 2./3. * (Pi + Pj - Pi*Pj / (Pi + Pj)) 
    rm = Pm/Zm/R/Tm
    b = 2*g*L*sina/Zm/R/Tm
    Hf = (Pi**2 * exp(-b) - Pj**2) * b / (1. - exp(-b)) / 2. / g / Pm / rm
    return Hf

Hf = funHf (Pi, Pj)

Pm = 2./3. * (Pi + Pj - Pi*Pj / (Pi + Pj))
rm = Pm/Zm/R/Tm
print("Pm = %g bar" % (Pm/1e5))
print("rm = %g kg/m^3" % rm)
print("Hf = %g m" % Hf)


if Hf > 0:
    print("escoamento flui de i para j")
elif Hf < 0:
    print("escoamento flui de j para i")
else:
    print("escoamento nulo")



print("\n" + 32*"-" + "\n1.2: qual a pressão de entrada limite?")


#dPij = newton (lambda x: funHf (Pj - x, Pj), 120)
b = 2*g*L*sina/Zm/R/Tm
Pi = Pj * exp(b/2.)
dPij = Pj - Pi

print("escoamento fica nulo para:")
print("dPij = %g Pa" % dPij)
print("Pi = %g Pa" % (Pj - dPij))
print("Pj = %g Pa" % Pj)




print("\n" + 32*"-" + "\n1.3: repetir 1.1 com Z variável")


def pontoCriticoGarb (dgas):
    """
    Method to estimate the pseudocritical conditions (Tpc, Ppc) through
    the relative density of the gas.
    Garb (1978) Petrol Eng Int, 50:p56, "Property Evaluation with Hand-held
    Calculators, Part II – Moving Averages and Gas Deviation Factor"
    """
    # dgas = RHOgas / RHOar
    Tpc = (167 + 316.67*dgas) * 5/9.  # K
    Ppc = (702.5 - 50*dgas)*6894.76  # Pa
    return Tpc, Ppc


def ZPapp (Tr, Pr):
    """
    Explicit method to compute Standing & Katz Z compressibility factor.
    Ref: Takacs (1989) "Comparing Methods for Calculating z Factor"
         Oil & Gas J, May 15, pp 43-46
    """
    x = Pr / Tr**2
    a = 0.1219 * Tr**0.638
    b = Tr - 7.76 + 14.75/Tr
    c = 0.3*x + 0.441*x**2
    return 1 + a*(x - b)*(1 - np.exp(-c))


Tpc, Ppc = pontoCriticoGarb (d)

def funHf (Pi, Pj):
    Pm = 2./3. * (Pi + Pj - Pi*Pj / (Pi + Pj))
    Zm = ZPapp (Tm/Tpc, Pm/Ppc)
    b = 2*g*L*sina/Zm/R/Tm
    rm = Pm/Zm/R/Tm
    Hf = (Pi**2 * exp(-b) - Pj**2) * b / (1. - exp(-b)) / 2. / g / Pm / rm
    return Hf

Hf = funHf (Pi, Pj)

Pm = 2./3. * (Pi + Pj - Pi*Pj / (Pi + Pj))
Zm = ZPapp (Tm/Tpc, Pm/Ppc)
rm = Pm /Zm/R/Tm

print("Tpc = %g K" % (Tpc))
print("Ppc = %g bar" % (Ppc/1e5))
print("Tr = %g" % (Tm/Tpc))
print("Pr = %g" % (Pm/Ppc))
print("Pm = %g bar" % (Pm/1e5))
print("Zm = %g" % Zm)
print("rm = %g kg/m^3" % rm)
print("Hf = %g m" % Hf)

if Hf > 0:
    print("escoamento flui de i para j")
elif Hf < 0:
    print("escoamento flui de j para i")
else:
    print("escoamento nulo")







