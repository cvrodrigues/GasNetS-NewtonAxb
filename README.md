# GasNetS-NewtonAxb

Didactic simulation of real gas-flow networks.


## Contents and usage

*GasNetS-NewtonAxb* is a repository of several Python scripts.

* `calcGasMixturesProperties.py` -- calculation thermodynamic characteristics
  for hydrocarbon-rich gas mixtures (e.g. natural gas) using real gas models.
  It encompasses several methods to compute the Standing & Katz *Z*
  compressibility factor.

* `exemploNewtonAxb_sistema*.py` -- simulation of real gas-flow networks using
  the `NewtonAxb` solver:
  - `exemploNewtonAxb_sistema1.py` solves a simple network of pipes arranged
    in series, using dry air.
  - `exemploNewtonAxb_sistema2.py` solves a network with both serial and parallel
    arrangements, also with dry air.
  - `exemploNewtonAxb_sistema3.py` extends the solver to a natural-gas mixture.
  - `exemploNewtonAxb_sistema4.py` solver for a complex network with 46 pipes,
    43 junctions, 2 producers and 21 consumers.
  - `exemploNewtonAxb_sistema5.py` extends the `exemploNewtonAxb_sistema3.py`
    to include potential energy due to ortographic height differences.

* `plotNetwork.py` - tool to visualize network graphs.

* `calcSoundSpeed.py` - routines to estimate the sound speed for real-gas
  mixtures.

* `testMixtures.py` - Testing suite for the `calcGasMixturesProperties.py`
  and `calcSoundSpeed.py`.

* folder `exercises` - contains simple gas-flow problems and their
  respective solution.

The scripts are mostly stand-alone, meaning all should be run independently
(though some may call routines defined in others).


## Software versions

The python scripts in *GasNetS-NewtonAxb* were developed and run using
the *Python 3* build of *Debian GNU/Linux 9.5 (stretch)*, namely:

* `Python 3.5.3, build: default, Jan 19 2017, 14:11:04, GCC 6.3.0 20170118`
* `numpy 1.14.5`
* `scipy 0.18.1`
* `matploblib 2.0.0`


## Copyright notice, software licenses and other notices

Copyright (C) 2018 Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>.  All rights reserved.

All files are under the GNU Lesser General Public License (version 3)
except for the following:
  * `plotNetwork.py` - under GNU General Public License (version 3)

Moreover, the `plotNetwork.py` script contains source-code adaptations from
project *netgraph.py* <http://github.com/paulbrodersen/netgraph>.

A copy of the GNU Lesser General Public License (version 3) may be found
in <https://www.gnu.org/licenses/> or the following file:
  * `LICENSE_LGPLv3.txt`

A copy of the GNU General Public License (version 3) may be found
in <https://www.gnu.org/licenses/> or the following file:
  * `LICENSE_GPLv3.txt`

