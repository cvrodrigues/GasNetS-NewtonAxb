#!/usr/bin/env python3
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :
#######
##
## calcGasMixturesProperties.py -- Routines to compute thermodynamic
##                                 thermodynamic properties of gas mixtures
##
## Author: Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>
## Copyright (C) 2018 Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>
## License: LGPLv3 - GNU Lesser General Public License, version 3
##          <http://www.gnu.org/licenses/lgpl-3.0.txt>
##
##   This program is free software: you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public License
##   as published by the Free Software Foundation, either version 3
##   of the License, or (at your option) any later version.
##  
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
##   GNU Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public License
##  along with this program. If not, see <https://www.gnu.org/licenses/>.
##
#######

from math import *
import numpy as np
import scipy as sp
from scipy.optimize import bisect, newton, brentq
import matplotlib as mpl
import matplotlib.pyplot as plt

eps = np.finfo(float).eps


## Constantes
R0 = 8314.41  # J/kmol/K
g = 9.80665
lb2kg = 0.45359237
lbf2N = lb2kg * g
in2mm = 25.4
psi2Pa = lbf2N / in2mm**2 * 1e6


libNome = {\
    'Metano': 'C1',\
    'Etano': 'C2',\
    'Propano': 'C3',\
    'Isobutano': 'iC4',\
    'n-Butano': 'nC4',\
    'Isopentano': 'iC5',\
    'n-Pentano': 'nC5',\
    'n-Hexano': 'nC6',\
    'n-Heptano': 'nC7',\
    'n-Octano': 'nC8',\
    'n-Nonano': 'nC9',\
    'n-Decano': 'nC10',\
    'n-Undecano': 'nC11',\
    'n-Dodecano': 'nC12',\
    'Etileno': 'C2H4',\
    'Propileno': 'C3H6',\
    'Azoto': 'N2',\
    'Dioxido de Carbono': 'CO2',\
    'Sulfureto de hidrogenio': 'H2S',\
    'Oxigenio': 'O2',\
    'Hidrogenio': 'H2',\
    'Agua': 'H2O',\
    'Air': 'Air',\
    'Argon': 'Arg',\
    'Helio': 'He',\
    }

## From: GPA Standard 2145-09 "Table of Physical Properties for Hydrocarbons
##       and Other  Compounds of Interest to the Natural Gas Industry"
##       Values at standard conditions of T=288.15 K and P=101325 Pa
libGas = {\
    'C1'  : {'M': 16.043,'Tpc':191,'Ppc':4599e3,'k':1.3075,'Cp':2203.6,'Cv':1685.3},\
    'C2'  : {'M': 30.070,'Tpc':305,'Ppc':4872e3,'k':1.1935,'Cp':1705.4,'Cv':1428.9},\
    'C3'  : {'M': 44.096,'Tpc':370,'Ppc':4251e3,'k':1.1318,'Cp':1619.1,'Cv':1430.5},\
    'iC4' : {'M': 58.122,'Tpc':408,'Ppc':3629e3,'k':1.0971,'Cp':1616.2,'Cv':1473.1},\
    'nC4' : {'M': 58.122,'Tpc':425,'Ppc':3796e3,'k':1.0949,'Cp':1651.1,'Cv':1508.0},\
    'iC5' : {'M': 72.149,'Tpc':460,'Ppc':3378e3,'k':1.0776,'Cp':1600.3,'Cv':1485.1},\
    'nC5' : {'M': 72.149,'Tpc':470,'Ppc':3370e3,'k':1.0765,'Cp':1621.6,'Cv':1506.4},\
    'nC6' : {'M': 86.175,'Tpc':507,'Ppc':3012e5,'k':1.0636,'Cp':1612.5,'Cv':1516.0},\
    'nC7' : {'M':100.202,'Tpc':540,'Ppc':2736e5,'k':1.0545,'Cp':1605.7,'Cv':1522.7},\
    'nC8' : {'M':114.229,'Tpc':569,'Ppc':2487e5,'k':1.0476,'Cp':1600.8,'Cv':1528.0},\
    'nC9' : {'M':128.255,'Tpc':595,'Ppc':2281e5,'k':1.0423,'Cp':1596.7,'Cv':1531.9},\
    'nC10': {'M':142.282,'Tpc':618,'Ppc':2103e5,'k':1.0381,'Cp':1593.8,'Cv':1535.4},\
    'nC11': {'M':156.302,'Tpc':639,'Ppc': 197e4,'k': None, 'Cp': None, 'Cv': None},\
    'nC12': {'M':170.338,'Tpc':658,'Ppc': 182e4,'k': None, 'Cp': None, 'Cv': None},\
    'C2H4': {'M': 28.053,'Tpc':282,'Ppc':5042e3,'k':1.2475,'Cp':1494.1,'Cv':1197.7},\
    'C3H6': {'M': 42.080,'Tpc':364,'Ppc':4555e3,'k':1.1527,'Cp':1491.6,'Cv':1294.0},\
    'CO2' : {'M': 44.010,'Tpc':304,'Ppc':7377e3,'k':1.2931,'Cp':833.40,'Cv':644.50},\
    'H2S' : {'M': 34.081,'Tpc':373,'Ppc':9000e3,'k':1.3237,'Cp':997.60,'Cv':753.60},\
    'N2'  : {'M': 28.013,'Tpc':126,'Ppc':3396e3,'k':1.3996,'Cp':1039.5,'Cv':742.70},\
    'O2'  : {'M': 32.000,'Tpc':155,'Ppc':5043e3,'k':1.3956,'Cp':916.60,'Cv':656.80},\
    'He'  : {'M': 4.0026,'Tpc':5.2,'Ppc':2275e2,'k':1.6667,'Cp':5193.1,'Cv':3115.8},\
    'Air' : {'M':28.9625,'Tpc':133,'Ppc':3786e3,'k':1.4002,'Cp':1004.5,'Cv':717.40},\
    'H2O' : {'M':18.0153,'Tpc':647,'Ppc':2206e4,'k':1.3295,'Cp':1862.0,'Cv':1400.5},\
    'Arg' : {'M':39.9480,'Tpc':151,'Ppc':4898e3,'k':1.6667,'Cp':520.33,'Cv':312.19},\
    'H2'  : {'M':2.01588,'Tpc': 33,'Ppc':1348e3,'k':1.4050,'Cp':14269.,'Cv':10156.},\
    }


def condutaCondicoesMedias (Ti, Pi, Tj, Pj):
    """
    Compute average conditions in pipes.
    Pi, Ti - conditions at the inlet cross-section
    Pj, Tj - conditions at the outlet cross-section
    """
    Tm = .5*(T1 + T2)
    Pm = 2./3.*(P1 + P2 - P1*P2/float(P1 + P2))
    return Tm, Pm


def pontoCriticoGarb (dgas):
    """
    Method to estimate the pseudocritical conditions (Tpc, Ppc) through
    the relative density of the gas.
    Garb (1978) "Moving Averages and Gas Deviation Factor", Petrol Eng Int 50:56-62
    In: "Property Evaluation With Handheld Computers" 
    dgas = RHOgas / RHOar
    """
    Tpc = (167 + 316.67*dgas) * 5/9.  # K
    Ppc = (702.5 - 50*dgas) * 6894.757293168361  # Pa
    return Tpc, Ppc


def pontoCriticoStanding (dgas, state="NaturalGas"):
    """
    dgas = RHOgas / RHOar
    """
    if "NaturalGas" == state:
        Tpc = (168 + 325*dgas - 12.5*dgas**2) * 5/9.  # K
        Ppc = (677 + 10*dgas - 37.5*dgas**2) * 6894.757293168361 # Pa
    elif "GasCondensate" == state:
        Tpc = (187 + 330*dgas - 71.5*dgas**2) * 5/9.  # K
        Ppc = (706 - 51.7*dgas - 11.1*dgas**2) * 6894.757293168361 # Pa
    return Tpc, Ppc


#######
##
## Metodos para calculo do factor Z
## Formulações implícitas
##
#######
def ZHallYarborough (Tr, Pr, tol=np.sqrt(np.finfo(float).eps), verbose=False):
    """
    Method to compute Standing & Katz Z compressibility factor.
    Ref: Hall Yarborough (1973) Oil & Gas J, 71:82–92
    http://github.com/cran/zFactor/blob/master/R/Hall-Yarborough.R
    """
    A = 0.06125 * Pr/Tr * np.exp(-1.2*(1 - 1./Tr)**2)
    B = (14.76 - 9.76/Tr + 4.58/Tr**2) / Tr
    C = (90.7 - 242.2/Tr + 42.4/Tr**2) / Tr
    D = 1.18 + 2.82/Tr
    Dp1 = D + 1
    ## Zfun1 and Zfun2 are the original functions of Hall & Yarborough
    Zfun1 = lambda y: A / y
    #Zfun2 = lambda y: (1 + y + y**2 - y**3)/(1. - y)**3 - B*y + C*y**D
    ## Function F(y) = y * (Zfun2(y) - Zfun1(y))
    F = lambda y: y*(1 + y + y**2 - y**3)/(1. - y)**3 - B*y**2 + C*y**Dp1 - A
    dF = lambda y: (1 + 4*y + 4*y**2 - 4*y**3 + y**4)/(1. - y)**4 \
        - 2*B*y + C*Dp1*y**D
    y = Zfun1(1.0)  # use Zfun1 to compute initial y, as A = y*Z
    #y = y/4.9  # as is done in cran/zFactor/blob/master/R/Hall-Yarborough.R
    y = newton(F, y, fprime=dF)
    Z = Zfun1(y)
    if verbose: print("y = \t%g\nZ = \t%g" % (y, Z)) ;
    return Z


def ZDranchukPurvisRobinson (Tr, Pr, tol=np.sqrt(np.finfo(float).eps)):
    """
    Method to compute Standing & Katz Z compressibility factor through the
    Benedict-Webb-Rubin (BWR) state equation for real gases.
    Ref: Dranchuk Purvis Robinson (1973) Paper CIM 73-112
         Proc 24th Annu Tech Meeting Petro Soc CIM, Canada, May 8–12
    http://github.com/cran/zFactor/blob/master/R/Dranchuk-Purvis-Robinson.R
    """
    A1 =  0.31506237
    A2 = -1.04670990
    A3 = -0.57832729
    A4 =  0.53530771
    A5 = -0.61232032
    A6 = -0.10488813
    A7 =  0.68157001
    A8 =  0.68446549
    T1 = A1 + A2/Tr + A3/Tr**3
    T2 = A4 + A5/Tr
    T3 = A5*A6/Tr
    T4 = A7/Tr**3
    Z_RHOr = 0.27*Pr/Tr  # Z*RHOr = 0.27 * Pr / Tr
    RHOr = 0.27*Pr/Tr  # initial guess with Z=1
    fun = lambda RHOr: 1 + T1*RHOr + T2*RHOr**2 + T3*RHOr**5 \
        + T4*RHOr**2 * (1 + A8*RHOr**2)*np.exp(-A8*RHOr**2) \
        - Z_RHOr/RHOr
    dfun = lambda RHOr: T1 + 2*T2*RHOr + 5*T3*RHOr**4 \
        + 2*T4*RHOr*np.exp(-A8*RHOr**2)\
        * ((1 + 2*A8*RHOr**2) - A8*RHOr**2 * (1 + A8*RHOr**2)) \
        + Z_RHOr/RHOr**2
    RHOr = newton(fun, RHOr, fprime=dfun, tol=tol)
    Z = 0.27*Pr/Tr/RHOr
    return Z


def ZDranchukAbouKassem (Tr, Pr, tol=np.sqrt(np.finfo(float).eps)):
    """
    Method to compute Standing & Katz Z compressibility factor through the
    Starling-Carnahan state equation for real gases.
    Ref: Dranchuk Abou-Kassem (1975) J Canadian Petrol Tech, 14(03):34-36
         doi: 10.2118/75-03-03
    http://github.com/cran/zFactor/blob/master/R/Dranchuk-AbouKassem.R
    """
    A = [0.3265, -1.0700, -0.5339, 0.01569, -0.05165, 0.5475, -0.7361,\
	0.1844, 0.1056, 0.6134, 0.7210]
    R1 = A[0] + A[1] / Tr + A[2] / Tr**3 + A[3] / Tr**4 + A[4] / Tr**5
    R2 = A[5] + A[6] / Tr + A[7] / Tr**2
    R3 = A[8] * (A[6] / Tr + A[7] / Tr**2)
    R4 = A[9] / Tr**3
    Z_RHOr = 0.27 * Pr / Tr  # Z*RHOr = 0.27 * Pr / Tr
    fun = lambda RHOr: R1 * RHOr + R2 * RHOr**2 - R3 * RHOr**5 \
        + R4 * RHOr**2 * (1 +  A[10]*RHOr**2) * exp(-A[10]*RHOr**2) + 1 \
        - Z_RHOr / RHOr
    dfun =  lambda RHOr: R1 + 2 * R2 * RHOr - 5 * R3 * RHOr**4 \
        + 2 * R4 * RHOr * exp(-A[10] * RHOr**2) \
        * ((1 +  2 * A[10]*RHOr**3) - A[10]*RHOr**2 * (1 +  A[10]*RHOr**2)) \
        + Z_RHOr / RHOr**2
    RHOr = 0.27 * Pr / Tr  # initial guess with Z=1
    RHOr = newton(fun, RHOr, fprime=dfun, tol=tol)
    Z = 0.27 * Pr / Tr / RHOr
    return Z


#######
##
## Metodos para calculo do factor Z
## Formulações explícitas
##
#######
def ZKralik (Tr, Pr):
    """
    Explicit method to compute Standing & Katz Z compressibility factor.
    Ref: "Evaluating Gas Network Capacities" (2015) eds Koch et al, SIAM
         Králik, Stiegler, Vostrý, Záworka (1988) "Dynamic Modeling of
         Large-Scale Networks with Application to Gas Distribution", Vol 6.
         Studies in Automation and Control series, Elsevier
    """
    return 1 + 0.257*Pr - 0.533*Pr/Tr


def ZPapp (Tr, Pr):
    """
    Explicit method to compute Standing & Katz Z compressibility factor.
    Ref: Takacs (1989) "Comparing Methods for Calculating z Factor"
         Oil & Gas J, May 15, pp 43-46
    """
    x = Pr / Tr**2
    a = 0.1219 * Tr**0.638
    b = Tr - 7.76 + 14.75/Tr
    c = 0.3*x + 0.441*x**2
    return 1 + a*(x - b)*(1 - np.exp(-c))


def ZSaleh (Tr, Pr):
    """
    Explicit method to compute Standing & Katz Z compressibility factor.
    Ref: "Evaluating Gas Network Capacities" (2015) eds Koch et al, SIAM
    """
    return 1 - 3.52*Pr*np.exp(-2.26*Tr) + 0.247*Pr**2*np.exp(-1.878*Tr)



#######
##
## Metodo para calculo da viscosidade
##
#######
def muCalc (T, RHO, R=None, M=None):
    """
    Method to compute viscosity of natural gases
    Lee Gonzalez Eakin (1966) J Petro Eng, 18:997-1800, doi:10.2118/1340-PA
    """
    if M is None:
        M = R0 / R
    elif R is None:
        R = R0 / M
    else:
        raise RuntimeError("Must specify R or M for the gas mixture!")
    K = (9.4 + 0.02*M)*sqrt((1.8*T)**3) / (209. + 19*M + 1.8*T)
    a =  3.5 + 986/1.8/T + 0.01*M
    b = 2.4 - 0.2*a
    mu = 1E-7 * K * exp(a*(1E-3*RHO)**b)
    return mu


#######
##
## Algoritmo de calculo
##
#######
def algoritmoCalcPropriedadesGas (fracoesMolares=None, RHOgas=None,\
    T=None, P=None, T1=None, P1=None, T2=None, P2=None):
    #######
    ##
    ## Condicoes operacionais
    ##
    #######
    if None is T1 and None is P1 and None is T2 and None is P2:
        print('Dados: valores de Pressao e Temperatura')
        print('\tCondicoes operacionais: T = %g K, P = %g Pa' % (T, P))
    else:
        print('Dados: valores para escoamento em conduta')
        print('\tT1, P1 - condicoes numa seccao a montante (e.g. entrada)')
        print('\tT2, P2 - condicoes numa seccao a jusante (e.g. saida)')
        print('\tT1 = %g K, P1 = %g Pa' % (T1, P1))
        print('\tT2 = %g K, P2 = %g Pa' % (T2, P2))
        ## Estimar condicoes operacionais
        T, P = condutaCondicoesMedias (T1, P1, T2, P2)
        print('\tCondicoes operacionais: T = %g K, P = %g Pa' % (T, P))
    #######
    ##
    ## Calculos
    ##
    #######
    if None is fracoesMolares and None is RHOgas:
        raise RuntimeError("Optar entre especificar (i) fraccao molar" \
            + " ou (ii) massa volumica da mistura.")
    elif not (None is RHOgas):
        ## Estimar ponto critico da mistura
        Rar = R0/libGas['Air']['M']
        RHOar = P/Rar/T
        dgas = RHOgas / RHOar
        Tpc, Ppc = pontoCriticoGarb (dgas)
        print("Tpc =\t%g K" % Tpc)
        print("Ppc =\t%g Pa" % Ppc)
        ## Propriedades reduzidas (ou pseudo-reduzidas) da mistura
        Tr = T/Tpc
        Pr = P/Ppc
        print("Tr  = \t%g" % Tr)
        print("Pr  = \t%g" % Pr)
        ## Calculo do factor Z pelo metodo Dranchuk-Purvis-Robinson
        # Z = ZKralik (Tr, Pr)
        Z = ZDranchukPurvisRobinson (Tr, Pr)
        print("Z   = \t%g" % Z)
        ## Massa volumica
        RHO = RHOgas
        print("RHO =\t%g kg/m**3" % RHO)
        R = P/RHO/T/Z
        M = R0 / R
        print("R   = \t%g J/kg/K" % R)
        print("M   = \t%g kg/kmol" % M)
    elif not (None is fracoesMolares):
        ## Verificar fraccao molar
        print("sum(Xi) =\t", sum([fracoesMolares[i] for i in fracoesMolares.keys()]))
        if not np.allclose(1, sum([fracoesMolares[i] for i in fracoesMolares.keys()])):
            print(72*'!\n' \
                + 'AVISO: sum(Xi) deve ser 1, rever fracções molares\n' \
                + 72*'!\n')
        ## M, R, Tpc e Ppc da mistura
        M = sum([libGas[i]['M'] * fracoesMolares[i] for i in fracoesMolares.keys()])
        R = R0 / M
        Tpc = sum([libGas[i]['Tpc'] * fracoesMolares[i] for i in fracoesMolares.keys()])
        Ppc = sum([libGas[i]['Ppc'] * fracoesMolares[i] for i in fracoesMolares.keys()])
        print("M   = \t%g kg/kmol" % M)
        print("R   = \t%g J/kg/K" % R)
        print("Tpc = \t%g K" % Tpc)
        print("Ppc = \t%g Pa" % Ppc)
        ## Propriedades reduzidas (ou pseudo-reduzidas) da mistura
        Tr = T/Tpc
        Pr = P/Ppc
        print("Tr  = \t%g" % Tr)
        print("Pr  = \t%g" % Pr)
        ## Calculo do factor Z
        Z = ZHallYarborough (Tr, Pr)
        print("Z   = \t%g" % Z)
        ## Massa volumica operacional
        RHO = P/R/T/Z
        print("RHO =\t%g kg/m**3" % RHO)
    ## Calculo viscosidade dinamica
    mu = muCalc (T, RHO, M=M)
    print("mu  = \t%g kg/m/s" % mu)
    return Z, T, P, RHO, R, mu, Tpc, Ppc


def testZcalc (Zfun):
    Tr = [1.05, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9,\
        2., 2.2, 2.4, 2.6, 2.8, 3.0]
    Pr = [0.5, 1., 1.5, 2., 2.5, 3., 3.5, 4., 4.5, 5., 5.5, 6., 6.5, 7.]
    Ztab = np.array([
        [0.829,0.589,0.253,0.280,0.343,0.407,0.471,0.534,0.598,0.663,0.727,0.786,0.846,0.904],\
        [0.854,0.669,0.426,0.369,0.393,0.440,0.500,0.557,0.615,0.673,0.729,0.784,0.841,0.897],\
        [0.893,0.779,0.657,0.554,0.519,0.534,0.565,0.607,0.650,0.695,0.741,0.789,0.841,0.891],\
        [0.916,0.835,0.756,0.684,0.638,0.624,0.633,0.653,0.684,0.719,0.759,0.802,0.844,0.892],\
        [0.936,0.874,0.816,0.764,0.727,0.707,0.705,0.716,0.734,0.760,0.792,0.828,0.865,0.903],\
        [0.948,0.900,0.859,0.822,0.794,0.776,0.770,0.777,0.790,0.810,0.836,0.863,0.892,0.924],\
        [0.959,0.923,0.888,0.860,0.839,0.824,0.816,0.818,0.829,0.846,0.868,0.892,0.918,0.946],\
        [0.968,0.941,0.914,0.892,0.876,0.863,0.857,0.856,0.864,0.879,0.897,0.918,0.942,0.966],\
        [0.974,0.952,0.933,0.917,0.905,0.896,0.891,0.893,0.901,0.913,0.929,0.947,0.967,0.988],\
        [0.978,0.960,0.945,0.933,0.924,0.917,0.916,0.917,0.924,0.935,0.949,0.966,0.985,1.005],\
        [0.982,0.969,0.956,0.947,0.941,0.937,0.937,0.939,0.945,0.955,0.969,0.986,1.003,1.021],\
        [0.989,0.980,0.973,0.967,0.963,0.961,0.963,0.968,0.976,0.987,1.000,1.014,1.029,1.044],\
        [0.993,0.988,0.984,0.981,0.980,0.980,0.983,0.990,0.999,1.011,1.023,1.036,1.049,1.062],\
        [0.997,0.995,0.994,0.994,0.994,0.996,1.000,1.007,1.016,1.026,1.038,1.050,1.062,1.074],\
        [0.999,1.000,1.002,1.005,1.008,1.011,1.016,1.022,1.030,1.039,1.049,1.059,1.069,1.081],\
        [1.002,1.006,1.009,1.013,1.018,1.023,1.029,1.034,1.041,1.048,1.056,1.065,1.075,1.086],\
        ])
    Z = []
    for t in Tr:
        Z.append([])
        for p in Pr:
            Z[-1].append( Zfun (t, p) )
    Z = np.array(Z)
    err = (Z/Ztab - 1) * 100
    print("max(|err|) = %g %%" % np.max(abs(err)))
    lev = np.linspace(-25,25,11)
    lev = [-20, -15, -10, -5, -1, 1, 5, 10, 15, 20]
    cmap = 'RdBu_r'
    cf = plt.contourf(Pr, Tr, err, lev, cmap=cmap, vmin=-20, vmax=20, extend='both')
    cb = plt.colorbar(cf, fraction=0.05, pad=0.05)
    ax = plt.gca()
    ax.set_xlabel("Temperatura pseudo-reduzida, $T_r$", fontsize="x-large")
    ax.set_ylabel("Pressão pseudo-reduzida, $P_r$", fontsize="x-large")
    ax.set_title("Erro de Z = %s ($T_r$, $P_r$) \n max(|$\\epsilon$|) = %.0f%%"\
        % (Zfun.__name__, np.max(abs(err))), fontsize="x-large")
    cb.ax.set_xlabel("$\\epsilon$ (%)", fontsize="large")
    cb.ax.xaxis.set_label_position('top') 
    plt.tight_layout()
    plt.subplots_adjust(left=.14, bottom=.07, right=.92, top=.93, wspace=0, hspace=0)
    fig = plt.gcf()
    fig.set_size_inches(5, 8, forward=True)
    plt.savefig("fig_erro_%s.pdf" % Zfun.__name__, pad_inches=0)
    plt.show()
    return



#######
##
## Execucao
##
#######
if __name__ == '__main__':
    print(7*'=' + ' Calculo das propriedades de uma mistura gasosa ')
    #######
    ##
    ## Dados
    ##
    #######
    ## fraccao molar do fluido
    fluido = {\
        'C1':  0.837,\
        'C2':  0.076,\
        'C3':  0.0192,\
        'iC4': 0.003,\
        'nC4': 0.004,\
        'iC5': 0.0008,\
        'nC5': 0.0009,\
        'nC6': 0.0008,\
        'N2':  0.054,\
        'CO2': 0.0023,\
        'He':  0.002,\
        }
    ## Hipotese 1: condicoes operacionais de Pressao e Temperatura
    # T = 15. + 273.15  # K
    # P = 4E5 + 1E5  # Pa, pressao manometrica + atmosferica
    ## Hipotese 2: escoamento em conduta
    ## T1, P1 - condicoes numa seccao a montante (e.g. entrada)
    ## T2, P2 - condicoes numa seccao a jusante (e.g. saida)
    T1 = 25. + 273.15  # K
    P1 = 5E5 + 1E5  # Pa, pressao manometrica + atmosferica
    T2 = 15. + 273.15  # K
    P2 = 0 + 1E5  # Pa, pressao manometrica + atmosferica
    #######
    ##
    ## Calculos
    ##
    #######
    print("\nTeste 1 - Calculo em conduta para gas sabendo composicao\n")
    Z, T, P, RHO, R, mu, Tpc, Ppc = algoritmoCalcPropriedadesGas (\
        fracoesMolares=fluido, T1=T1, P1=P1, T2=T2, P2=P2)
    print("\nTeste 2 - Calculo local para gas onde sabemos RHOgas\n")
    Z, T, P, RHO, R, mu, Tpc, Ppc = algoritmoCalcPropriedadesGas (\
        RHOgas=RHO, T=T, P=P)
    print(7*'=' + ' Fim ')
    #######
    ##
    ## Avaliar erros no calculo de Z
    ##
    #######
    for fun in [ZHallYarborough, ZDranchukPurvisRobinson, ZDranchukAbouKassem,\
        ZKralik, ZPapp, ZSaleh]:
        testZcalc(fun)


