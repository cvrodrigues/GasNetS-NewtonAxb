#!/usr/bin/env python3
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :
#######
##
## exemploNewtonAxb_sistema4.py  -- didactic simulation of gas-flow networks
##
## Author: Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>
## Copyright (C) 2018 Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>
## License: LGPLv3 - GNU Lesser General Public License, version 3
##          <http://www.gnu.org/licenses/lgpl-3.0.txt>
##
##   This program is free software: you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public License
##   as published by the Free Software Foundation, either version 3
##   of the License, or (at your option) any later version.
##  
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
##   GNU Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public License
##  along with this program. If not, see <https://www.gnu.org/licenses/>.
##
#######

from math import *
import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import newton, fsolve, root, minimize, basinhopping, leastsq


eps = np.finfo(float).eps
tol = np.sqrt(eps)
Runiv = 8314.41  # J/kmol/K
Mar = 28.9625  # kg/kmol

cmp = lambda x, y: int(x > y) - int(x < y)

def pontoCriticoGarb (dgas):
    """
    Method to estimate the pseudocritical conditions (Tpc, Ppc) through
    the relative density of the gas.
    Garb (1978) Petrol Eng Int, 50:p56, "Property Evaluation with Hand-held
    Calculators, Part II – Moving Averages and Gas Deviation Factor"
    """
    # dgas = RHOgas / RHOar
    Tpc = (167 + 316.67*dgas) * 5/9.  # K
    Ppc = (702.5 - 50*dgas)*6894.76  # Pa
    return Tpc, Ppc

def ZDranchukPurvisRobinson (Tr, Pr, tol=np.sqrt(np.finfo(float).eps)):
    """
    Method to compute Standing & Katz Z compressibility factor through the
    Benedict-Webb-Rubin (BWR) state equation for real gases.
    Ref: Dranchuk Purvis Robinson (1973) Paper CIM 73-112
         Proc 24th Annu Tech Meeting Petro Soc CIM, Canada, May 8–12
    http://github.com/cran/zFactor/blob/master/R/Dranchuk-Purvis-Robinson.R
    """
    A1 =  0.31506237
    A2 = -1.04670990
    A3 = -0.57832729
    A4 =  0.53530771
    A5 = -0.61232032
    A6 = -0.10488813
    A7 =  0.68157001
    A8 =  0.68446549
    T1 = A1 + A2/Tr + A3/Tr**3
    T2 = A4 + A5/Tr
    T3 = A5*A6/Tr
    T4 = A7/Tr**3
    Z_RHOr = 0.27*Pr/Tr  # Z*RHOr = 0.27 * Pr / Tr
    RHOr = 0.27*Pr/Tr  # initial guess with Z=1
    fun = lambda RHOr: 1 + T1*RHOr + T2*RHOr**2 + T3*RHOr**5 \
        + T4*RHOr**2 * (1 + A8*RHOr**2)*np.exp(-A8*RHOr**2) \
        - Z_RHOr/RHOr
    dfun = lambda RHOr: T1 + 2*T2*RHOr + 5*T3*RHOr**4 \
        + 2*T4*RHOr*np.exp(-A8*RHOr**2)\
        * ((1 + 2*A8*RHOr**2) - A8*RHOr**2 * (1 + A8*RHOr**2)) \
        + Z_RHOr/RHOr**2
    RHOr = newton(fun, RHOr, fprime=dfun, tol=tol)
    Z = 0.27*Pr/Tr/RHOr
    return Z

def condutaCondicoesMedias (T1, P1, T2, P2):
    """
    Compute average conditions in pipes.
    Pi, Ti - conditions at the inlet cross-section
    Pj, Tj - conditions at the outlet cross-section
    """
    Tm = .5*(T1 + T2)
    Pm = 2./3.*(P1 + P2 - P1*P2/float(P1 + P2))
    return Tm, Pm

def muCalc (T, RHO, R=None, M=None, Runiv=8314.41):
    """
    Method to compute viscosity of natural gases
    Lee Gonzalez Eakin (1966) J Petro Eng, 18:997-1800, doi: 10.2118/1340-PA
    """
    if M is None and R is None:
        raise RuntimeError("Must specify R or M for the gas mixture!")
    if M is None:
        M = Runiv / R
    K = (9.4 + 0.02*M)*sqrt((1.8*T)**3) / (209. + 19*M + 1.8*T)
    a =  3.5 + 986/1.8/T + 0.01*M
    b = 2.4 - 0.2*a
    mu = 1E-7 * K * exp(a*(1E-3*RHO)**b)
    return mu

def factorFriccao (m, D, Tm, RHOm, E=0, R=None):
    def f_colebrook (Re, E, D):
        fun = lambda f, Re, E, D: \
            1./np.sqrt(f) + 2*np.log10(E/D/3.7 + 2.51/Re/np.sqrt(f))
        f0 = newton(fun, 2e-2, args=(Re, E, D,), tol=tol)
        return f0
    def f_haaland (Re, E, D):
        fun = lambda f, Re, E, D: \
            1./np.sqrt(f) + 1.8*np.log10((E/D/3.7)**1.11 + 6.9/Re)
        return (-1.8*np.log10((E/D/3.7)**1.11 + 6.9/Re))**(-2)
    mu = muCalc (Tm, RHOm, R=R)
    Re = 4 * abs(m) / np.pi / D / mu
    ## Basic Heaviside
    #if Re <= 2300:
    #    f = 64. / Re
    #else:
    #    f = f_colebrook (Re, E, D)
    #    #f = f_haaland (Re, E, D)
    ## interpolate between both equations using a sigmoid
    # sigmoid = lambda x: 1. / (1. + np.exp(-x))
    # a = sigmoid(-10*((Re - 2e3)/(4e3 - 2e3) - .5)*10)
    # f = a * 64./Re + (1 - a) * f_colebrook (Re, E, D)
    ## interpolate using a cosine
    if Re <= 2000:
        f = 64. / Re
    elif Re >= 4000:
        f = f_colebrook (Re, E, D)
        #f = f_haaland (Re, E, D)
    else:
        a = .5*(np.cos(np.pi*(Re - 2e3)/(4e3 - 2e3)) + 1)
        f = a * 64./Re + (1 - a) * f_colebrook (Re, E, D)
    return f


#######
##
## Dados do problema
##
#######

baseZ, baseT, baseP, baseQ = 1.0, 300.0, 1e5, 1.0

dgas = 0.65
Mgas = dgas * Mar
R = Runiv / Mgas
Tpc, Ppc = pontoCriticoGarb (dgas)
Ppc *= 1e-5 
Z = ZDranchukPurvisRobinson (baseT/Tpc, baseP*1e-5/Ppc)


Pmin, Pmax = 3.0, None  # in bars
#Pmin, Pmax = 3.0, 80.  # in bars

connection = {\
    '02': {'f':0, 't':2, 'D':80e-3, 'L':3e3, 'roughness': 0.046e-3},\
    '13': {'f':1, 't':3, 'D':80e-3, 'L':2e3, 'roughness': 0.046e-3},\
    '24': {'f':2, 't':4, 'D':60e-3, 'L':1e3, 'roughness': 0.046e-3},\
    '25': {'f':2, 't':5, 'D':60e-3, 'L':1e3, 'roughness': 0.046e-3},\
    '35': {'f':3, 't':5, 'D':60e-3, 'L':1e3, 'roughness': 0.046e-3},\
    '36': {'f':3, 't':6, 'D':60e-3, 'L':1e3, 'roughness': 0.046e-3},\
    #
    '04': {'f':0, 't':4, 'D':80e-3, 'L':4e3, 'roughness': 0.046e-3},\
    '05': {'f':0, 't':5, 'D':80e-3, 'L':4e3, 'roughness': 0.046e-3},\
    '15': {'f':1, 't':5, 'D':80e-3, 'L':2e3, 'roughness': 0.046e-3},\
    '16': {'f':1, 't':6, 'D':80e-3, 'L':2e3, 'roughness': 0.046e-3},\
    #
    '440': {'f':4, 't':40, 'D':30e-3, 'L':1e3, 'roughness': 0.046e-3},\
    '4041': {'f':40, 't':41, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    '4142': {'f':41, 't':42, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    #
    '220': {'f':2, 't':20, 'D':30e-3, 'L':1e3, 'roughness': 0.046e-3},\
    '2021': {'f':20, 't':21, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    '2122': {'f':21, 't':22, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    #
    '550': {'f':5, 't':50, 'D':30e-3, 'L':1e3, 'roughness': 0.046e-3},\
    '5051': {'f':50, 't':51, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    '5152': {'f':51, 't':52, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    #
    '330': {'f':3, 't':30, 'D':30e-3, 'L':1e3, 'roughness': 0.046e-3},\
    '3031': {'f':30, 't':31, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    '3132': {'f':31, 't':32, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    #
    '660': {'f':6, 't':60, 'D':30e-3, 'L':1e3, 'roughness': 0.046e-3},\
    '6061': {'f':60, 't':61, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    '6162': {'f':61, 't':62, 'D':30e-3, 'L':.5e3, 'roughness': 0.046e-3},\
    #
    '4043': {'f':40, 't':43, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '4145': {'f':41, 't':45, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '4247': {'f':42, 't':47, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    #    
    '2023': {'f':20, 't':23, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '2024': {'f':20, 't':24, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '2125': {'f':21, 't':25, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '2126': {'f':21, 't':26, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '2227': {'f':22, 't':27, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '2228': {'f':22, 't':28, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    #
    '5053': {'f':50, 't':53, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '5155': {'f':51, 't':55, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '5257': {'f':52, 't':57, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    #    
    '3033': {'f':30, 't':33, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '3034': {'f':30, 't':34, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '3135': {'f':31, 't':35, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '3136': {'f':31, 't':36, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '3237': {'f':32, 't':37, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '3238': {'f':32, 't':38, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    #
    '6063': {'f':60, 't':63, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '6165': {'f':61, 't':65, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    '6267': {'f':62, 't':67, 'D':20e-3, 'L':.2e3, 'roughness': 0.046e-3},\
    }

producer = {\
    '10000': {'n':0, 'Q': None},\
    '10001': {'n':1, 'Q': None},\
    }

consumer = {\
    '20043': {'n':43, 'Q': 25e-3},\
    '20045': {'n':45, 'Q': 25e-3},\
    '20047': {'n':47, 'Q': 25e-3},\
    #
    '20023': {'n':23, 'Q': 25e-3},\
    '20024': {'n':24, 'Q': 25e-3},\
    '20025': {'n':25, 'Q': 25e-3},\
    '20026': {'n':26, 'Q': 25e-3},\
    '20027': {'n':27, 'Q': 25e-3},\
    '20028': {'n':28, 'Q': 25e-3},\
    #
    '20053': {'n':53, 'Q': 37e-3},\
    '20055': {'n':55, 'Q': 37e-3},\
    '20057': {'n':57, 'Q': 37e-3},\
    #
    '20033': {'n':33, 'Q': 25e-3},\
    '20034': {'n':34, 'Q': 25e-3},\
    '20035': {'n':35, 'Q': 25e-3},\
    '20036': {'n':36, 'Q': 25e-3},\
    '20037': {'n':37, 'Q': 25e-3},\
    '20038': {'n':38, 'Q': 25e-3},\
    #
    '20063': {'n':63, 'Q': 25e-3},\
    '20065': {'n':65, 'Q': 25e-3},\
    '20067': {'n':67, 'Q': 25e-3},\
    }

#######
##
## Calculos
##
#######
## Check input data is allright
for s in connection.keys():
    ## check for roughness in connections list
    if not 'roughness' in list(connection[s].keys()):
        connection[s]['roughness'] = None
    if connection[s]['roughness'] is None:
        connection[s]['roughness'] = 0.0
        #connection[s]['roughness'] = 1e-5 * D   
    ## check for local losses in connections list
    if not 'drag' in list(connection[s].keys()):
        connection[s]['drag'] = None
    if connection[s]['drag'] is None:
        connection[s]['drag'] = 0.0

for k in producer.keys():
    if not 'Q' in list(producer[k].keys()):
        producer[k]['Q'] = None
    if not producer[k]['Q'] is None:
        print("Warning: Producer %s, a value for 'Q' was set")
        print("         This value will be disregarded in the calculations")

for k in consumer.keys():
    if not 'Q' in list(consumer[k].keys()):
        consumer[k]['Q'] = None
    if not 'm' in list(consumer[k].keys()):
        consumer[k]['m'] = None
    if consumer[k]['Q'] is None and consumer[k]['m'] is None:
        raise RuntimeError("Consumer %s, mandatory to set either 'Q' or 'm'"%k)


## Get number of elements and dicts to order keys
Nconn = len(connection.keys())
connectionKeys = sorted(connection.keys())

Nc = len(consumer.keys())
consumerKeys = sorted(consumer.keys())

Np = len(producer.keys())
producerKeys = sorted(producer.keys())


## Get all nodes that compose the grid
nodes = []
for s in connectionKeys:
    i, j = connection[s]['f'], connection[s]['t']
    nodes.extend([i, j])

nodes = list(set(nodes))  # a 'set' keeps only unique elements
Nnodes = len(nodes)

def getNodeIndex (n):
    return np.flatnonzero(np.array(nodes) == n)[0]


## Initial fields
T = baseT + np.zeros(Nnodes)
P = Pmin + np.zeros(Nnodes)
Z = np.array([ZDranchukPurvisRobinson (T[n]/Tpc, P[n]/Ppc)\
    for n in range(Nnodes)])

## Use global mass conservation to set flow in producers
mo = 0.0
for k in consumerKeys:
    if consumer[k]['Q'] is None:
        mo += consumer[k]['m']
    else:
        n = getNodeIndex(consumer[k]['n'])
        consumer[k]['m'] = consumer[k]['Q'] * P[n]*1e5/Z[n]/R/T[n]
        mo += consumer[k]['m']

print("global mass outflow = %g kg/s" % mo)
m = mo / float(Np)
for k in producerKeys:
    producer[k]['m'] = m
    #producer[k]['Q'] = producer[k]['m'] / (P[n]*1e5/Z[n]/R/T[n])


## Estimate initial field for the mass flow from the outlet flow
m = mo / float(Nconn)  # average flow
#m = mo  # max flow
for k in connectionKeys:
    connection[k]['m'] = m



#######
##
## Plot da rede
##
#######
from plotNetwork import makeAdjacency
kwargs = {'s':'conn', 'p':'p', 'c':'c', 'sf':'f', 'st':'t', 'sm':'m',\
    'pj':'n', 'cj':'n', 'node_size':5, 'node_label_font_size':6, 'edge_width':2}
_ = makeAdjacency ({'conn':connection, 'p':producer, 'c':consumer},\
    plot=True, **kwargs)
plt.show()
plt.close('all')



#######
##
## Algorithm
##
#######

def funCij (s):
    i, j = connection[s]['f'], connection[s]['t']
    i, j = getNodeIndex(i), getNodeIndex(j)
    D, L = connection[s]['D'], connection[s]['L']
    m = connection[s]['m']
    Tm, Pm = condutaCondicoesMedias (T[i], P[i], T[j], P[j])
    Zm = ZDranchukPurvisRobinson (Tm/Tpc, Pm/Ppc)
    ZRT = Zm * R * Tm
    RHOm = Pm*1e5 / ZRT
    #f = 0.03
    connection[s]['friction_factor'] = \
        factorFriccao (abs(m)+eps, D, Tm, RHOm, E=connection[s]['roughness'], R=R)
    floss = connection[s]['friction_factor'] * L / D
    floss += connection[s]['drag']
    a = ZRT / (np.pi*D**2/4.)**2 * floss
    return 1e10 / a

def setConnectionsFlow (mij):
    for (s, k,) in zip(connectionKeys, range(Nconn)):
        connection[s]['m'] = mij[k]
    return

def getConnectionsFlow ():
    mij = []
    for s in connectionKeys:
        mij.append( connection[s]['m'] )
    return np.array(mij)

def setProducersFlow (mp):
    for (n, k,) in zip(producerKeys, range(Np)):
        producer[n]['m'] = mp[k]
    return

def getProducersFlow ():
    mp = []
    for k in producerKeys:
        mp.append( producer[k]['m'] )
    return np.array(mp)

def getConsumersFlow ():
    mc = []
    for k in consumerKeys:
        mc.append( consumer[k]['m'] )
    return np.array(mc)

def updateConnectionsFlow ():
    for s in connectionKeys:
        i, j = connection[s]['f'], connection[s]['t']
        i, j = getNodeIndex(i), getNodeIndex(j)
        connection[s]['Cij'] = funCij(s)
        mij2 = connection[s]['Cij'] * (P2[i] - P2[j])
        connection[s]['m'] = np.sign(mij2) * np.sqrt(np.abs(mij2))
    return

def updateConsumersFlow ():
    for k in consumerKeys:
        n = getNodeIndex(consumer[k]['n'])
        if not consumer[k]['Q'] is None:
            consumer[k]['m'] = consumer[k]['Q'] * P[n]*1e5/Z[n]/R/T[n]
    return

def correctProducersLocalFlow ():
    ## Correct mass flow based on local net flow
    for k in producerKeys:
        n = producer[k]['n']
        m = 0.0
        for s in connectionKeys:
            i, j = connection[s]['f'], connection[s]['t']
            if n == i:
                m += connection[s]['m']
            if n == j:
                m -= connection[s]['m']
        producer[k]['m'] = m  # m is what is leaving the node
        producer[k]['m'] = max(0, m)  # allow inflow only
    return

def computeGlobalInOutflow ():
    mi, mo = 0.0, 0.0
    ## Consumers
    for k in consumerKeys:
        mo += consumer[k]['m']
    ## Producers
    for k in producerKeys:
        mi += producer[k]['m']
    return mi, mo

def correctProducersGlobalFlow (mi, mo):
    ## Correct flow on producer nodes to ensure global mass conservation
    for k in producerKeys:
        if abs(mi) > 0:
            producer[k]['m'] = producer[k]['m'] / mi * mo
        else:
            producer[k]['m'] = mo / float(Np)
    return

def computeLocalMassResid ():
    ## Local mass residuals (inflow is negative, outflow is positive)
    localMassResid = np.zeros(Nnodes)
    for s in connectionKeys:
        i, j = connection[s]['f'], connection[s]['t']
        i, j = getNodeIndex(i), getNodeIndex(j)
        # node is at the start, flow is leaving
        localMassResid[i] += connection[s]['m']
        # node is at the final, flow is entering
        localMassResid[j] -= connection[s]['m']
    for k in producerKeys:
        n = getNodeIndex(producer[k]['n'])
        localMassResid[n] -= producer[k]['m']  # flow is always entering
    for k in consumerKeys:
        n = getNodeIndex(consumer[k]['n'])
        localMassResid[n] += consumer[k]['m']  # flow is always leaving
    return np.array(localMassResid)

def computeGlobalMassResid ():
    ## Global mass residual (inflow is negative, outflow is positive)
    mresg = 0.0
    mi, mo = computeGlobalInOutflow ()
    return mo - mi

def computeConnectionsResid ():
    localConnResid = []
    for s in connectionKeys:
        i, j = connection[s]['f'], connection[s]['t']
        i, j = getNodeIndex(i), getNodeIndex(j)
        connection[s]['Cij'] = funCij(s)
        mij2 = connection[s]['m'] * abs(connection[s]['m'])
        localConnResid.append(mij2 - connection[s]['Cij'] * (P2[i] - P2[j]))
    return np.array(localConnResid)

def rootFun (P2):
    P2 += -P2.min() + Pmin**2
    P[:] = np.sqrt(np.abs(P2))  # unsquare P field
    A = np.zeros((Nnodes, Nnodes))
    b = np.zeros((Nnodes))
    for s in connectionKeys:
        i, j = connection[s]['f'], connection[s]['t']
        i, j = getNodeIndex(i), getNodeIndex(j)
        m = connection[s]['m']
        dij = abs(cmp(m, 0))  # returns 0 if m == 0, 1 otherwise
        cij = funCij (s) * dij / (abs(m) + eps)
        ## i is centre, j is neighbor
        A[i][j] = - cij
        A[i][i] += cij
        ## j is centre, i is neighbor
        A[j][i] = - cij
        A[j][j] += cij
    #
    for k in producerKeys:
        n = getNodeIndex(producer[k]['n'])
        b[n] = producer[k]['m']
    #
    for k in consumerKeys:
        n = getNodeIndex(consumer[k]['n'])
        b[n] = - consumer[k]['m']
    #
    ## Normalize matrix and independent vector
    #Aii = np.dot(A * np.eye(A.shape[0]), np.ones(A.shape[0]))
    Aii = np.diagonal(A)
    A = A / Aii[:,None]
    b = b / Aii
    #
    ## Under-relaxation
    urf = 0.5
    for i in range(A.shape[0]):
        A[i,i] /= urf
        b[i] += A[i,i]*(1-urf) * P[i]**2
    return np.dot(A, P2) - b

objFun = lambda P2: np.abs(rootFun(P2))

objScalarFun = lambda P2: np.sum(rootFun(P2)**2)


def solver (x0):
    """
    Set up the solver to the objective function. It may be one of the
    following methods:
      * Root-finding algorithm
      * Local optimization, minimizer of a vector of scalars
      * Local optimization, minimizer of a scalar function
      * Local optimization, minimizer of a scalar function with bounds
      * Local optimization, minimizer of a scalar function with constraints
      * Global optimization with basinhopping
    Reference to the methods:
    http://docs.scipy.org/doc/scipy-0.18.1/reference/optimize.html
    http://docs.scipy.org/doc/scipy-0.18.1/reference/tutorial/optimize.html
    """
    ## Default root-finding algorithm
    x, _, success, _ = fsolve(rootFun, x0, full_output=True)
    #
    ## Root-finding algorithm
    ## 
    # sol = root(rootFun, x0, method="hybr")
    # sol = root(rootFun, x0, method="lm")
    # sol = root(rootFun, x0, method="krylov")
    # sol = root(rootFun, x0, method="df-sane")
    # x, success = sol.x, int(sol.success)
    #
    ## Local optimization: minimizer of a vector of scalar
    # sol = leastsq(objFun, x0, full_output=True)
    # x, success = sol[0], 1 <= sol[-1] <= 4
    #
    ## Local optimization: minimizer of a scalar functions
    # sol = minimize(objScalarFun, x0, method="Nelder-Mead")
    # sol = minimize(objScalarFun, x0, method="Powell")
    # sol = minimize(objScalarFun, x0, method="CG")
    # sol = minimize(objScalarFun, x0, method="BFGS")
    # sol = minimize(objScalarFun, x0, method="L-BFGS-B")
    # x, success = sol.x, int(sol.success)
    #
    ## Local optimization, minimizer of a scalar function with bounds
    # bounds = tuple([(Pmin, Pmax) for n in range(Nnodes)])
    # sol = minimize(objScalarFun, x0, method="L-BFGS-B", bounds=bounds)
    # sol = minimize(objScalarFun, x0, method="SLSQP", bounds=bounds)
    # x, success = sol.x, int(sol.success)
    #
    ## Local optimization, minimizer of a scalar function with constraints
    # constraints = []
    # if Pmin is not None:
    #     constraints.append(\
    #         {'type': 'ineq', 'fun': lambda P2: min(P2) - Pmin**2})
    # if Pmax is not None:
    #     constraints.append(\
    #         {'type': 'ineq', 'fun': lambda P2: Pmax**2 - max(P2)})
    # sol = minimize(objScalarFun, x0, method="SLSQP", constraints=constraints)
    # x, success = sol.x, int(sol.success)
    #
    ## Global Optimization algorithm
    ##  - with L-BFGS-B + bounds
    # bounds = tuple([(Pmin, Pmax) for n in range(Nnodes)])
    # kwargs = {"method":"L-BFGS-B", "bounds":bounds}
    #
    ##  - with SLSQP + bounds + constraints
    # bounds = tuple([(Pmin, Pmax) for n in range(Nnodes)])
    # constraints = ()
    # kwargs = {"method":"SLSQP", "bounds":bounds, "constraints":constraints}
    #
    # sol = basinhopping(objScalarFun, x0, niter=200, minimizer_kwargs=kwargs)
    # x, success = sol.x, int(sol.lowest_optimization_result.success)
    return x, success


def solverLoop ():
    global Z, P, P2
    ## Prepare
    Z = np.array([ZDranchukPurvisRobinson (T[n]/Tpc, P[n]/Ppc)\
        for n in range(Nnodes)])
    P2 = P[:]**2
    #updateConnectionsFlow ()            # compute mass flows in connections
    updateConsumersFlow ()               # update consumers whose flow is m^3/s
    #correctProducersLocalFlow ()         # enforce local mass balance
    mi, mo = computeGlobalInOutflow ()   # compute global inflow and outflow
    correctProducersGlobalFlow (mi, mo)  # enforce global mass balance
    ## Loop
    mdifl = []
    converged = False
    niter = 0
    while True:
        niter +=1
        P2, success = solver (P2)            # call solver
        ## Apply restrictions
        P2 += -P2.min() + Pmin**2            # certify minimum P is equal to Pmin 
        P[:] = np.sqrt(P2)                   # unsquare P field
        Z = np.array([ZDranchukPurvisRobinson (T[n]/Tpc, P[n]/Ppc)\
            for n in range(Nnodes)])
        updateConnectionsFlow ()             # calc mass flow in connections
        #updateConsumersFlow ()              # update consumers in m^3/s units
        #correctProducersLocalFlow ()         # enforce local mass balance
        mi, mo = computeGlobalInOutflow ()   # compute global inflow/outflow
        correctProducersGlobalFlow (mi, mo)  # enforce global mass balance
        mresl = computeLocalMassResid ()     # local mass residual
        mresl = max(np.abs(mresl))
        mresg = abs(mo - mi)                 # global mass residual 
        #
        print("iter %8d" % niter \
            + "  success %s" % success \
            + "  mass resid: glob= %.3e kg/s" % mresg\
            + "  loc= %.3e kg/s  (%.2e %%)" % (mresl, mresl/mo*1e2))
        if max(mresg, mresl) < tol:
            print("Solution converged: absolute error below tol")
            converged = True
            break
        if max(mresg/mo, mresl/mo) < tol:
            print("Solution converged: relative error below tol")
            converged = True
            break
        ## Stop if solution does not change
        if len(mdifl) < 100:
            mdifl.append(mresl)
        elif niter < 200:
            _ = mdifl.pop(0)
            mdifl.append(mresl)
        else:
            dmdifl = [abs(m - mresl) for m in mdifl]
            if max(dmdifl) < tol:
                print("Warning: Solution local residuals not changing")
                print("         max residual    = %.10e kg/s" % max(mdifl))
                print("         max difference  = %.10e kg/s" % max(np.abs(dmdifl)))
                print("         global residual = %.10e kg/s" % mresg)
                print("         local residual  = %.10e kg/s" % mresl)
                break
            _ = mdifl.pop(0)
            mdifl.append(mresl)
        ## Stop by iteration number
        if niter >= 2000:
            print("Warning: Reached maximum iterations allowed, exiting")
            break
        ## Print intermediate solutin
        if 1 == niter % 100:
            print("\n\tintermediate solution")
            print("\tP    = ", P)
            print("\tmij  = ", getConnectionsFlow ())
            print("\tf    = ", max([connection[s]['friction_factor'] \
                for s in connectionKeys]))
            print("")
    return converged



def printSolution ():
    print("\n*******  Solution")
    mresl = computeLocalMassResid ()     # local mass residual
    print("mass residual: %g kg/s", max(np.abs(mresl)))
    print("T   = \t", T - 273.15, " degC")
    print("P   = \t", P, " bar")
    print("RHO = \t", P*1e5/Z/R/T, " kg/m^3")
    print("Z   = \t", Z)
    #
    for k in producerKeys:
        print("\n+++ producer %s" % k)
        n = getNodeIndex(producer[k]['n'])
        RHO = P[n]*1e5 / Z[n] / R / T[n]
        if producer[k]['Q'] is None:
            Q = producer[k]['m'] / RHO * 1e3
        else:
            Q = producer[k]['Q'] * 1e3
        print("\tQ= %g L/s  m= %g kg/s  P= %g bar" % (Q,producer[k]['m'],P[n]))
    #
    for k in consumerKeys:
        print("\n--- consumer %s" % k)
        n = getNodeIndex(consumer[k]['n'])
        RHO = P[n]*1e5 / Z[n] / R / T[n]
        if consumer[k]['Q'] is None:
            Q = consumer[k]['m'] / RHO * 1e3
        else:
            Q = consumer[k]['Q'] * 1e3
        print("\tQ= %g L/s  m= %g kg/s  P= %g bar" % (Q,consumer[k]['m'],P[n]))
    #
    for s in connectionKeys:
        print("\n=== connection %s" % s)
        i, j = connection[s]['f'], connection[s]['t']
        i, j = getNodeIndex(i), getNodeIndex(j)
        Tm, Pm = condutaCondicoesMedias (T[i], P[i], T[j], P[j])
        Zm = ZDranchukPurvisRobinson (Tm/Tpc, Pm/Ppc)
        D, L = connection[s]['D'], connection[s]['L']
        RHOm = Pm*1e5 / Zm / R / Tm
        f = connection[s]['friction_factor']
        floss = f*L/D + connection[s]['drag']
        print("\tPm = %g bar   Tm = %g degC   RHOm = %g kg/m^3   feq = %g"\
            % (Pm, Tm - 273.15, RHOm, floss))
        print("\tQ = %g L/s  m = %g kg/s  f = %.4e"\
            % (connection[s]['m'] / RHOm * 1e3, connection[s]['m'], f))
    print("\n")
    return


## First try
solverLoop ()
printSolution ()



#######
##
## Plot da rede
##
#######
from plotNetwork import makeAdjacency, plotAdjacency
kwargs = {'s':'conn', 'p':'p', 'c':'c', 'sf':'f', 'st':'t', 'sm':'m',\
    'pj':'n', 'cj':'n'}
adjacency, label, color, posxy = makeAdjacency (\
    {'conn':connection, 'p':producer, 'c':consumer}, plot=False, **kwargs)

nodesxy = {\
    0:[2,3], 1:[2,-3],\
    2:[7,3], 3:[7,-3], 4:[7,6], 5:[7,0], 6:[7,-6],\
    40:[10, 6], 41:[12, 6], 42:[14, 6],\
    20:[10, 3], 21:[12, 3], 22:[14, 3],\
    50:[10, 0], 51:[12, 0], 52:[14, 0],\
    30:[10,-3], 31:[12,-3], 32:[14,-3],\
    60:[10,-6], 61:[12,-6], 62:[14,-6],\
    43:[ 9, 5], 45:[11, 5], 47:[13, 5],\
    23:[10, 4], 25:[12, 4], 27:[14, 4],\
    24:[10, 2], 26:[12, 2], 28:[14, 2],\
    53:[11, 1], 55:[13,-1], 57:[13, 1],\
    33:[10,-4], 35:[12,-4], 37:[14,-4],\
    34:[10,-2], 36:[12,-2], 38:[14,-2],\
    63:[ 9,-5], 65:[11,-5], 67:[13,-5],\
    }

posxy = {}
for k in label.keys():
    n = label[k]
    posxy[k] = np.array(nodesxy[n]) / np.array([12., 12.])

_ = plotAdjacency (adjacency,\
    node_labels=label, node_color=color, node_positions=posxy,\
    node_size=3, node_label_font_size=8, edge_width=0.8, edge_cmap='RdGy_r')
plt.tight_layout()
plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
fig = plt.gcf()
fig.set_size_inches(6, 6, forward=True)
plt.savefig("fig_exemplo4.pdf", pad_inches=0)
plt.show()
plt.close('all')

_ = plotAdjacency (adjacency,\
    node_labels=label, node_color=color, node_positions=posxy,\
    node_size=3, node_label_font_size=8, edge_width=0.8, edge_color='gray')
plt.tight_layout()
plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
fig = plt.gcf()
fig.set_size_inches(6, 6, forward=True)
plt.savefig("fig_exemplo4_gray.pdf", pad_inches=0)
plt.show()
plt.close('all')





